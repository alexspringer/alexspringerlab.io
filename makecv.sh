#!/bin/bash
pandoc -o public/resume.html -c public/resume-css-stylesheet.css source/resume.md
pandoc -o public/cc_sales.html -c public/resume-css-stylesheet.css source/cc_sales.md
pandoc -o public/index.html -c public/resume-css-stylesheet.css source/letter_template.md
pandoc -o public/user_guide.html -c public/resume-css-stylesheet.css source/user_guide.md
pandoc -o letters/cover_letter.html -c public/resume-css-stylesheet.css letters/working_letter.md
wkhtmltopdf public/resume.html ASpringerResume.pdf
wkhtmltopdf letters/cover_letter.html ASpringerCoverLetter.pdf
